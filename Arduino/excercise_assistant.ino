// NOTES
// -commented out all BLE stuff to test filtering

//#include <ArduinoBLE.h>
#include <Arduino_LSM9DS1.h>

//BLEService BTservice("180A"); // BLE LED Service
//// BLE LED Switch Characteristic - custom 128-bit UUID, read and writable by central
//BLEByteCharacteristic input("2A57", BLERead | BLEWrite);
////BLECharacteristic gyroscope("2A60", BLERead, 30, (1==1));
//BLECharacteristic acceleration("2A60", BLERead, 30, (1==1));

String output;
char buffer[30];

typedef struct accelerometerOutput{
  float pitch;
  float roll;
} AccOut;

//Get, filter, transmit Accelometer samples
bool updateAccelerometer() {
  //--- TODO --- Eventually turn this into a hardware interrupt
  float x, y, z;
  IMU.readAcceleration(x, y, z);

  AccOut pitchRoll;
  pitchRoll.pitch = getPitch(x, y, z);
  pitchRoll.roll = getRoll(y, z);
  
  pitchRoll = filterSample(pitchRoll);

  output = "";
  output += (pitchRoll.pitch);
  output += ":";
  output += (pitchRoll.roll);
  Serial.println(output);
  
//  output.toCharArray(buffer, 30);
//  acceleration.writeValue(buffer); 

  return true;      
}


//Get pitch
float getPitch(float xa, float ya, float za){
  float pitch= asin( xa/sqrt(xa*xa+ya*ya+za*za) );
  //If z-acceleration is less than 0, the sensor is upside down
  if(za < 0){
      pitch = PI - pitch;
  }
  return pitch;
}

//Get roll
float getRoll(float ya, float za){
  float roll = atan(ya/za);
  //If z-acceleration is less than 0, the sensor is upside down
  if(za < 0){
    roll = PI + roll;
  }
  return roll;
}

//filters [pitch, roll] data array with progressive averaging filter
#define SAMPLES 50
AccOut filterSample(AccOut data){
  //Put the data into an array for processing at a new or the least recently accessed location
  static int index;
  static float dataArray[SAMPLES][2];
  float sample[2];
  sample[0] = data.pitch;
  sample[1] = data.roll;

  for(int type = 0 ; type < 2; type++){
    dataArray[index][type] = sample[type];
  }
  //Increment index so we don't just write the same value at the same location over and over
  index++;
  if(index == SAMPLES){
    index = 0;
  }

  //Summing all data in the array to get the final average-filtered sample
  float filtered[2];
  for(int type = 0; type < 2; type++){
    //Resetting
    filtered[type] = 0;
    //Summing
    for(int i=0; i < SAMPLES; i++){
      filtered[type] += dataArray[i][type];
    }
    //Averaging
    filtered[type] = filtered[type]/SAMPLES;     
  }
  data.pitch = filtered[0];
  data.roll = filtered[1];
  
  //Return array of average of last 'SAMPLES' amount of samples
  return data;
}

void setup() {
  Serial.begin(9600);
  while (!Serial);

  // set LED's pin to output mode
  pinMode(LEDB, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  digitalWrite(LED_BUILTIN, LOW);         // when the central disconnects, turn off the LED
  digitalWrite(LEDB, HIGH);                // will turn the LED off

  // begin initialization
//  if (!BLE.begin()) {
//    Serial.println("starting Bluetooth® Low Energy failed!");
//    while (1);
//  }

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }

  // set advertised local name and service UUID:
//  BLE.setLocalName("Nano 33 BLE Sense");
//  BLE.setAdvertisedService(BTservice);
//  // add the characteristic to the service
//  BTservice.addCharacteristic(input);
//  BTservice.addCharacteristic(acceleration);
//  // add service
//  BLE.addService(BTservice);
//  // set the initial value for the characteristic:
//  input.writeValue(0);
//  acceleration.writeValue("None");
//  // start advertising
//  BLE.advertise();
}

void loop() {
  if(IMU.accelerationAvailable() ){
    updateAccelerometer();
  }
  

//-----------------------------------------------------------------Commented all the bluetooth stuff, sorry Juho
//  // listen for Bluetooth® Low Energy peripherals to connect:
//  BLEDevice central = BLE.central();
//
//  // if a central is connected to peripheral:
//  if (central) {
//    Serial.print("Connected to central: ");
//    // print the central's MAC address:
//    Serial.println(central.address());
//    digitalWrite(LED_BUILTIN, HIGH);            // turn on the LED to indicate the connection
//
//    // while the central is still connected to peripheral:
//    while (central.connected()) {
//      updateAccelometer();
//      // Turn blue led on
//      digitalWrite(LEDB, LOW);
//      // if the remote device wrote to the characteristic,
//      // use the value to control the LED:
//      if (input.written()) {
//        switch (input.value()) {   // any value other than 0
//          case 01:
//            Serial.println(output);
//            break;
//          default:
//            break;
//        }
//      } else if (Serial.read() != -1) { //Accept also serial input to print output
//        Serial.println(output);
//      }
//    }
//    // when the central disconnects, print it out:
//    Serial.print(F("Disconnected from central: "));
//    Serial.println(central.address());
//    digitalWrite(LED_BUILTIN, LOW);         // when the central disconnects, turn off the LED
//    digitalWrite(LEDB, HIGH);         // will turn the LED off
//  } else {
//    updateAccelometer();
//
//    if (Serial.read() != -1) {
//      Serial.println(output);
//    }
//  }
//---------------------------------------------------------------------Until here

}
