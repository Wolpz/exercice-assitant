#include <ArduinoBLE.h>
#include <Arduino_LSM9DS1.h>

BLEService BTservice("180A"); // BLE LED Service
// BLE LED Switch Characteristic - custom 128-bit UUID, read and writable by central
BLEByteCharacteristic input("2A57", BLERead | BLEWrite);
BLECharacteristic gyroscope("2A60", BLERead, 30, (1==1));
BLECharacteristic acceleration("2A61", BLERead, 30, (1==1));

String gyro, acc;
float gyroX, gyroY, gyroZ;
float accX, accY, accZ;
char buffer[30];

void setup() {
  Serial.begin(9600);
  while (!Serial);

  // set LED's pin to output mode
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  digitalWrite(LED_BUILTIN, LOW);         // when the central disconnects, turn off the LED
  digitalWrite(LEDR, HIGH);               // will turn the LED off
  digitalWrite(LEDG, HIGH);               // will turn the LED off
  digitalWrite(LEDB, HIGH);                // will turn the LED off

  // begin initialization
  if (!BLE.begin()) {
    Serial.println("starting Bluetooth® Low Energy failed!");
    while (1);
  }

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }

  // set advertised local name and service UUID:
  BLE.setLocalName("Nano 33 BLE Sense");
  BLE.setAdvertisedService(BTservice);
  // add the characteristic to the service
  BTservice.addCharacteristic(input);
  BTservice.addCharacteristic(gyroscope);
  BTservice.addCharacteristic(acceleration);
  // add service
  BLE.addService(BTservice);
  // set the initial value for the characteristic:
  input.writeValue(0);
  gyroscope.writeValue("None");
  acceleration.writeValue("None");
  // start advertising
  BLE.advertise();
}

void loop() {
  // listen for Bluetooth® Low Energy peripherals to connect:
  BLEDevice central = BLE.central();

  // if a central is connected to peripheral:
  if (central) {
    Serial.print("Connected to central: ");
    // print the central's MAC address:
    Serial.println(central.address());
    digitalWrite(LED_BUILTIN, HIGH);            // turn on the LED to indicate the connection

    // while the central is still connected to peripheral:
    while (central.connected()) {
      digitalWrite(LEDB, LOW);
      // Get gyroscope values
      if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(gyroX, gyroY, gyroZ);
        gyro = "Gyroscope: {";
        gyro += "x: ";
        gyro += gyroX;
        gyro += " y: ";
        gyro += gyroY;
        gyro += " z: ";
        gyro += gyroZ;
        gyro += "} EOL";

        gyro.toCharArray(buffer, 30);
        gyroscope.writeValue(buffer);
        //Serial.println(buffer);        
      }
      if (IMU.accelerationAvailable()) {
        IMU.readAcceleration(accX, accY, accZ);
        acc = "Acceleration: {";
        acc += "x: ";
        acc += accX;
        acc += " y: ";
        acc += accY;
        acc += " z: ";
        acc += accZ;
        acc += "} EOL";

        acc.toCharArray(buffer, 30);
        acceleration.writeValue(buffer);
        //Serial.println(buffer);        
      }

      // if the remote device wrote to the characteristic,
      // use the value to control the LED:
      if (input.written()) {
        switch (input.value()) {   // any value other than 0
          case 01:
            break;
          default:
            break;
        }
      }
    }

    // when the central disconnects, print it out:
    Serial.print(F("Disconnected from central: "));
    Serial.println(central.address());
    digitalWrite(LED_BUILTIN, LOW);         // when the central disconnects, turn off the LED
    digitalWrite(LEDR, HIGH);          // will turn the LED off
    digitalWrite(LEDG, HIGH);        // will turn the LED off
    digitalWrite(LEDB, HIGH);         // will turn the LED off
  }
}