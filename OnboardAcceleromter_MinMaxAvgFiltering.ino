#include <Arduino_LSM9DS1.h>
#define SAMPLES 50

float axis_x;
float axis_y;
float axis_z;
float x, y, z, xmax, ymax, zmax = 0;
float xmin, ymin, zmin = 100;
float deg_xz, deg_yx, deg_zy = 0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  while(!IMU.begin() ){
    Serial.println("Starting IMU...");
  }
  Serial.println("IMU started succesfully.");
  Serial.println("x y z");
}

void loop() {
  // put your main code here, to run repeatedly:
  x = 0; y = 0; z = 0;

  
  for(int i = 0; i < SAMPLES; i++){
    while(!IMU.accelerationAvailable() );
    xmax = 0; ymax = 0; zmax = 0;
    xmin = 100; ymin = 100; zmin = 100;
    
    IMU.readAcceleration(axis_x, axis_y, axis_z);

//    String s = "-Intermediate- ";
//    s += "x: ";
//    s += axis_x;
//    s += " y: ";
//    s += axis_y;
//    s += " z: "; 
//    s += axis_z;
    

    x += axis_x;
    y += axis_y;
    z += axis_z;
    
    // Peak filtering for x
    if(xmax < axis_x){
      xmax = axis_x;
    }
    if(xmin > axis_x){
      xmin = axis_x;
    }
    // Peak filtering for y
    if(ymax < axis_y){
      ymax = axis_y;
    }
    if(ymin > axis_y){
      ymin = axis_y;
    }
    // Peak filtering for z
    if(zmax < axis_z){
      zmax = axis_z;
    }
    if(zmin > axis_z){
      zmin = axis_z;
    }

//    s += " xmami: ";
//    s += xmax;
//    s += " ";
//    s += xmin;
//    s += " ymmami: ";
//    s += ymax;
//    s += " ";
//    s += ymin;
//    s += " zmami: ";
//    s += zmax;
//    s += " ";
//    s += zmin;    
//    Serial.println(s);
  }
  
//  axis_x = (x-xmin-xmax)/(SAMPLES-2);
//  axis_y = (y-ymin-ymax)/(SAMPLES-2);
//  axis_z = (z-zmin-zmax)/(SAMPLES-2);
  
  axis_x = (x)/(SAMPLES);
  axis_y = (y)/(SAMPLES);
  axis_z = (z)/(SAMPLES);
  
//  deg_yx = 180 + atan(axis_x/axis_y) / (2*PI) * 360;
//  deg_xz = 180 + atan(axis_z/axis_y) / (2*PI) * 360;
//  deg_zy = 180 + atan(axis_y/axis_z) / (2*PI) * 360;



//  String output = "x:";
//  output += (deg_xz);
//  output += " y:";
//  output += (deg_yx);
//  output += " z:";
//  output += (deg_zy);
//  Serial.println(output); 

  String output = "";
  output += (axis_x);
  output += ":";
  output += (axis_y);
  output += ":";
  output += (axis_z);
  Serial.println(output); 

}
